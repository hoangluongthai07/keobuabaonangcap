﻿using System;

internal class Program
{
    static void Main()
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;

        Console.WriteLine("Trò chơi Kéo Búa Bao!");
        Console.WriteLine("Người chơi 1 đưa ra lựa chọn: ");
        Console.WriteLine("1 - Kéo, 2 - Búa, 3 - Bao");

        int player1Choice = int.Parse(Console.ReadLine());

        Console.WriteLine("Người chơi 2 đưa ra lựa chọn: ");
        Console.WriteLine("1 - Kéo, 2 - Búa, 3 - Bao");

        int player2Choice = int.Parse(Console.ReadLine());

        string result = DetermineWinner(player1Choice, player2Choice);
        Console.WriteLine(result);
    }

    static string DetermineWinner(int player1Choice, int player2Choice)
    {
        switch (player1Choice)
        {
            case 1:
                return GetResult(1, player2Choice);

            case 2:
                return GetResult(2, player2Choice);

            case 3:
                return GetResult(3, player2Choice);

            default:
                return "Lựa chọn không hợp lệ";
        }
    }

    static string GetResult(int player1Choice, int player2Choice)
    {
        if (player1Choice == player2Choice)
        {
            return "Kết quả: Hòa!";
        }
        else if ((player1Choice == 1 && player2Choice == 2) ||
                 (player1Choice == 2 && player2Choice == 3) ||
                 (player1Choice == 3 && player2Choice == 1))
        {
            return "Kết quả: Người chơi 1 thắng!";
        }
        else
        {
            return "Kết quả: Người chơi 2 thắng!";
        }
    }
}
